import asyncio
import pytest

from pyrmx.monero import handlers
from pyrmx.monero.utils import (
    AsyncBatchManager, AsyncBlockManager, BatchIndex)
from pyrmx.pb_schemas import monero_pb2

@pytest.mark.asyncio
async def test_getting_global_blockheight(monero_rpc):
    c = {
        'xmr_node': monero_rpc
    }

    request = monero_pb2.OutInitScanning()
    request.batch_size = 1024

    resp_byte, resp = await handlers.handle_init_scanning(request, c)

    assert resp_byte == b'\x85'
    assert resp.global_blockheight > 0

    assert c['xmr_scanning_batch_size'] == 1024


@pytest.mark.asyncio
async def test_handle_sending_xmr_transaction_batches(monero_rpc):
    c = {
        'xmr_node': monero_rpc,
        'xmr_scanning_batch_size': 1024
    }

    request = monero_pb2.OutGetTransactionBatch()
    request.local_blockheight = 1
    request.offset = 0

    resp_byte, resp = await handlers.handle_sending_xmr_transaction_batches(
        request, c)

    assert resp_byte == b'\x87'
    # same as request
    assert resp.start_blockheight == 1
    assert resp.start_tx_offset  == 0

    # the last transaction of the batch
    # it's hard to test explicitly (if the stagenet is restarted)
    assert resp.end_blockheight == 4
    assert resp.end_tx_offset == 0

    # the next blockheight is either the same as end_blockheight and
    # end_tx_offset + 1
    # or
    # end_blockheight + 1 and 0 (for next_tx_offset)
    # this depends whether the last transaction in batch is the last
    # transaction in given block
    assert resp.next_blockheight == 5
    assert resp.next_tx_offset == 0

    assert resp.global_blockheight > 0



@pytest.mark.parametrize('req,expected_type',
[
    # coinbase one output
    (
        monero_pb2.OutGetTransactionDetails(
            batch_blockheight=0, batch_tx_offset=0, want_tx_offset=0),
        monero_pb2.InGetTransactionDetails.TransactionType.COINBASE
    ),
    # coinbase one output
    (
        monero_pb2.OutGetTransactionDetails(
            batch_blockheight=1000, batch_tx_offset=0, want_tx_offset=1),
        monero_pb2.InGetTransactionDetails.TransactionType.COINBASE
    ),
    (
        monero_pb2.OutGetTransactionDetails(
            batch_blockheight=446344, batch_tx_offset=0, want_tx_offset=1),
        monero_pb2.InGetTransactionDetails.TransactionType.RCT_4
    )
])

@pytest.mark.asyncio
async def test_handling_get_transaction_details(
        req, expected_type, monero_rpc):
    c = {
        'xmr_node': monero_rpc,
        'xmr_batch_manager': AsyncBatchManager(AsyncBlockManager(monero_rpc, None)),
        'xmr_scanning_batch_size': 16384
    }
    resp_byte, resp = await handlers.handle_get_transaction_details(req, c)
    assert resp_byte == b'\x89'
    assert resp.type == expected_type
    assert len(resp.Tx_data) == c['xmr_scanning_batch_size']

@pytest.mark.parametrize(
    'key,spent',
    [
        (b'ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff', 0),
        (b'508a41cbfa3cb66fc2013b65055f6e00893cf984570dda70a4eaaf4bc2182e0b', 1)

    ]
)
@pytest.mark.asyncio
async def test_handling_is_key_spent(key, spent, monero_rpc):
    c = {'xmr_node': monero_rpc}
    request = monero_pb2.OutIsSpent()
    request.key = key

    resp_byte, resp = await handlers.handle_is_spent(request, c)
    assert resp_byte == b'\x93'
    assert resp.spent == spent
