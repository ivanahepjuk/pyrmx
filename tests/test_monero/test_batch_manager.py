import pytest
from pyrmx.monero.utils import (AsyncBlockManager,
                                AsyncBatchManager,
                                BatchIndex)

@pytest.mark.asyncio
async def test_batch_manager_creating_batch(monero_rpc):
    blocks = AsyncBlockManager(monero_rpc)
    batches = AsyncBatchManager(blocks)

    batch_size = 1024
    first_tx_index = BatchIndex(0, 0)
    batches.schedule_batch(first_tx_index, size=batch_size)
    batch = await batches.get_batch(first_tx_index, size=batch_size)
    first_batch_tx = batch[0]
    assert first_batch_tx.block_height == 0
    assert first_batch_tx.tx_offset == 0
    batch2 = await batches.get_batch(
        batch.next_batch_index,
        size=batch_size,
        prefetch_next=True)

    assert len(batch2) > 0
    batch2_first_tx = batch2[0]
    assert (
        (batch2_first_tx.block_height, batch2_first_tx.tx_offset)
        == batch.next_batch_index
    )

    assert len(batches._future_batches) == 1
    list(batches._future_batches.values())[0].cancel()
    blocks._bg_refresh_global_blockheight_job.cancel()
