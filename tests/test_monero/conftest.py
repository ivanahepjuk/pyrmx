import pytest
import asyncio

from pyrmx.monero.node import PyrmxMoneroClient

@pytest.yield_fixture()
async def monero_rpc():
    c = PyrmxMoneroClient('http://127.0.0.1:38081')
    yield c
    await c.close()

