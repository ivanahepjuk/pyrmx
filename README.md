# PyRMX - a client for communicating with the RMX hardware wallet
## Installation

While in development stage, instalation from source is required. 
after cloning the repo, run
```bash
$ git clone https://gitlab.com/rmxwallet/pyrmx/
$ cd pyrmx
$ python3 -m pip install . --upgrade
```

## Usage

### Running monero synchronisation
```sh
$ python3 -m pyrmx --monerod http://127.0.0.1:18081 -v
```
the `-v` flag adds debug messages

(see `python3 -m pyrmx --help` for available flags)

Then button 7 on rmx

We assume locally running monerod on stagenet, for this go to getmonero.org, download latest release (command line version is enough) and run it in separate terminal with stagenet switch as 

```sh
$ ./monerod --stagenet
```
### Interactive use
You can now use the client in your own scripts for interactive debugging

```
$ python3
>>> from pyrmx.client import RmxClient
>>> rmx = RmxClient(monerod_url='http://127.0.0.1:18081')
>>> rmx.interact()
```


## Development

run tests (from the root of the repo) with 
```sh
$ python3 -m pytest
```
(assuming `pytest` is installed with `$ python3 -m pip install pytest`)

Updating protobuf schemas (assuming `protoc` is installed), manually for now. When things get stable use or something more robust!

```sh
$ cp ~/projects/rmx-hardware-wallet/sources/nanopb/*.proto ./pb_schemas
$ find pyrmx/pb_schemas -name '*.proto' | xargs protoc --python_out=pyrmx/pb_schemas
```
