"""Messages are binary blobs of data exchanged with rmx hw over USB
"""
import struct
import logging

from pyrmx.utils import checksum
from pyrmx.device import RMXDevice
from pyrmx.exceptions import MalformedDataError, RmxHwError

logger = logging.getLogger(__name__)
CMD_START = b"\x81"


def _ensure_bytes(x, size=None):
    if isinstance(x, int):
        x = [x]
    # try:
    b = bytes(x)
    if size is not None and len(b) != size:
        raise ValueError("Expected {} bytes, got {!r} in {!r}".format(size, len(b), b))
    return b

    # except:
    #     raise ValueError(
    #         "Can't convert {!r} (type {}) to bytes".format(x, type(x)))


class Message:
    def __init__(
        self,
        command_byte,
        payload_size,
        payload_bytes,
        checksum,
        start_byte=CMD_START,
        raw_blob=None,
        payload=None,
    ):
        # logger.debug("start_byte %s", start_byte)
        self.start_byte = _ensure_bytes(start_byte, 1)
        # logger.debug("command_byte %s", command_byte)
        self.command_byte = _ensure_bytes(command_byte, 1)
        # logger.debug("payload_size %s", payload_size)
        if isinstance(payload_size, int):
            payload_size = payload_size.to_bytes(2, byteorder="big")
        elif not isinstance(payload_size, bytes):
            raise ValueError(
                "payload_size must be int encoded as 2-bytes or int, "
                "not {!r} {}".format(payload_size, type(payload_size))
            )
        self.payload_size = payload_size
        # optionally contains parsed payload into whatever is appropriate
        # (list, dict, protobuf,...)
        # logger.debug("payload %s", payload)
        self.payload = payload
        # logger.debug("payload_bytes %s", payload_bytes)
        self.payload_bytes = _ensure_bytes(payload_bytes)
        # logger.debug("checksum %s", checksum)
        self.checksum = checksum
        self.raw_blob = raw_blob

    @classmethod
    def from_binary(cls, blob):
        """Parse byte[string] blob into a message"""
        # logger.debugvv("Building message from binary blob=%s", blob)
        start_byte = blob[0]
        command_byte = blob[1]
        payload_size = struct.unpack(">H", blob[2:4])[0]
        payload_bytes = blob[4 : 4 + payload_size]
        ix_checksum_start = 4 + payload_size
        checksum = blob[ix_checksum_start : ix_checksum_start + 8]
        message = cls(
            start_byte=start_byte,
            command_byte=command_byte,
            payload_size=payload_size,
            payload_bytes=payload_bytes,
            checksum=checksum,
            raw_blob=blob,
        )
        message.verify()
        return message

    @classmethod
    def build(cls, command_byte, payload_bytes=None, payload=None):
        """Build a message of binary data that RMX hw can understand

        the message contains a header and a checksum
        Args:
            command_byte (bytes): single byte or an int representing the command
            payload_bytes (bytes, bytearray): actual payload
        """

        if isinstance(command_byte, int):
            command_byte = bytes([command_byte])

        if payload_bytes is None and payload is None:
            raise ValueError("pass one of `payload_bytes` or `payload`")
        if payload_bytes is not None and not isinstance(
            payload_bytes, (bytes, bytearray)
        ):
            raise TypeError(
                "payload_bytes must be bytes or bytearray object, not {}".format(
                    type(payload_bytes)
                )
            )
        elif payload is not None:
            # assume it's proto message
            payload_bytes = payload.SerializeToString()

        # payload_size encoded as 2 byte integer
        payload_size = struct.pack(">H", len(payload_bytes))

        message = cls(
            command_byte=command_byte,
            payload_size=payload_size,
            payload_bytes=payload_bytes,
            # Payload is not really needed, but to make inspecting
            # the message  easier. Might this be a source of inconsistency?
            payload=payload,
            checksum=checksum(payload_bytes),
        )
        # just to be sure
        message.verify()
        return message

    def verify(self):
        if self.start_byte != CMD_START:
            raise MalformedDataError(
                "Invalid start byte {!r}".format(self.start_byte),
                raw_blob=self.raw_blob,
            )
        if self.checksum != checksum(self.payload_bytes):
            raise MalformedDataError(
                "Checksum doesn't tally. Got {!r} expected {!r}".format(
                    self.checksum, checksum(self.payload_bytes)
                ),
                raw_blob=self.raw_blob,
            )
        return True

    def as_bytes(self):
        return b"".join(
            [
                CMD_START,
                self.command_byte,
                self.payload_size,
                self.payload_bytes,
                checksum(self.payload_bytes),
            ]
        )

    def __repr__(self):
        return "{}({!r})".format(self.__class__.__name__, self.__dict__)
