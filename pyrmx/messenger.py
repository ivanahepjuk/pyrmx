# -*- coding: utf-8 -*-

from binascii import hexlify, unhexlify
from collections import defaultdict
import asyncio
import logging
import time
from operator import itemgetter
from itertools import groupby
from collections import deque


import slixmpp
from pyrmx.pb_schemas.xmpp_pb2 import (
    OutXMPPLogin,
    InXMPPLoginResponse,
    OutAddContact,
    InAddContact,
    InQueryUnread,
    OutQueryUnread,
    OutQueryMessage,
    InMessage,
    OutMessageAck,
    OutMessage,
    InMessageAck,
)

from pyrmx.pyrmxd import register_handler
from slixmpp.plugins.xep_0054.stanza import VCardTemp
from pyrmx.utils import NB

logger = logging.getLogger(__name__)


class PyRMXClientXMPP(slixmpp.ClientXMPP):

    """
    A simple Slixmpp bot that will echo messages it
    receives, along with a short thank you message.
    """

    def __init__(self, jid, password):
        super().__init__(jid, password)

        # The session_start event will be triggered when
        # the bot establishes its connection with the server
        # and the XML streams are ready for use. We want to
        # listen for this event so that we we can initialize
        # our roster.
        self.message_q = asyncio.Queue()
        self.header_stats = {}
        # self.add_event_handler("session_start", self.start)

        # The message event is triggered whenever a message
        # stanza is received. Be aware that that includes
        # MUC messages and error messages.
        self.add_event_handler("message", self.message)
        self.register_plugin("xep_0313")
        self.register_plugin("xep_0013")
        self.register_plugin("xep_0059")
        self.register_plugin("xep_0054")

    def start(self, event):
        """
        Process the session_start event.

        Typical actions for the session_start event are
        requesting the roster and broadcasting an initial
        presence stanza.

        Arguments:
            event -- An empty dictionary. The session_start
                     event does not provide any additional
                     data.
        """
        # self.send_presence()
        # self.get_roster()

    async def message(self, msg):
        """
        Process incoming message stanzas. Be aware that this also
        includes MUC messages and error messages. It is usually
        a good idea to check the messages's type before processing
        or sending replies.

        Arguments:
            msg -- The received message stanza. See the documentation
                   for stanza objects and the Message stanza to see
                   how it may be used.
        """
        await self.message_q.put(msg)

    async def get_offline_message_headers(self):
        headers = await self.plugin["xep_0013"].get_headers()
        # does this introduce unnecessary state?
        self.header_stats = defaultdict(list)

        for mfrom, msgs in groupby(
            # the headers['disco_items']['items'] is a list of tuples
            # ('thehood@jabb.im', -- to_jid (self)
            # '6837701', -- node_id used to query the message
            # 'thesheriff@jabb.im/793958580478815948931980838' --from_jid)
            sorted(headers["disco_items"]["items"], key=itemgetter(2, 1)),
            key=itemgetter(2),
        ):
            # we want to take the bare JID, dont care about /resource
            bare_from_jid = mfrom.split("/")[0]
            self.header_stats[bare_from_jid].extend(map(itemgetter(1), msgs))

        logger.debugv("Got offline messages %s", self.header_stats)
        for v in self.header_stats.values():
            v.sort(reverse=True)
        return self.header_stats

    def format_offline_message_headers_into_counts(self):
        encoded = []
        for jid, msgs in self.header_stats.items():
            encoded.append(jid.encode("utf8") + b";" + len(msgs).to_bytes(1, "little"))
        unpadded = b";".join(encoded) + b";"
        # defined in pyrmx/pb_schemas/xmpp.options
        if len(unpadded) > 1024:
            raise ValueError("TODO handle too many unread messages")
        return unpadded.ljust(1024, NB)

    @staticmethod
    def _do_nothing(iq):
        pass

    async def get_message_for_jid(self, jid, delete=False):
        # handle missing jid/no messages for jid
        if not self.header_stats:
            await self.get_offline_message_headers()

        jid = jid.split("/")[0]
        message_id = self.header_stats[jid].pop()
        self.plugin["xep_0013"].view(nodes=message_id, callback=self._do_nothing)
        # calling this actually receives the message in the backend
        # and calls the registered `handle_message` callback
        # which pushes the incoming message to the self.message_q
        msg = await self.message_q.get()
        if delete:
            logger.debugvv("Deleting message %s", message_id)
            deleted = await self.remove_offline_message(message_id)
            if deleted:
                logger.debugvv("Deleted")
            else:
                logger.debug("Couldn't delete message %s", message_id)
        return msg

    async def remove_offline_message(self, node):
        self.plugin["xep_0013"].remove(nodes=node, callback=self._do_nothing)
        return True


def encode_message(message: bytes, metadata: bytes):
    """

    metadata is always 32Bytes (one time random)*G so we can just slap it at the end
    """
    return hexlify(message + metadata).decode("utf8")


def decode_message(hex_msg: bytes):
    """"""
    raw_blob = unhexlify(hex_msg)
    msg = raw_blob[:-32]
    metadata = raw_blob[-32:]
    if len(msg) != 256:
        raise ValueError(
            "raw message {!r} should be 256 bytes not {}".format(msg, len(msg))
        )
    if len(metadata) != 32:
        raise ValueError(
            "metadata {!r} should be 32 bytes not {}".format(metadata, len(metadata))
        )

    return msg, metadata


@register_handler(b"\x40", OutXMPPLogin)
async def handle_OutXMPPLogin(msg, c):
    """handle incoming xmpp login request

    create tcp connection
    publish pubkey to my vcard roster
    """
    # msg.jid
    # msg.passwd
    # msg.pubkey

    try:
        xmpp_session = c["xmpp_session"]
        if not xmpp_session.is_connected():
            xmpp_session.reconnect()
    except KeyError:
        xmpp_session = PyRMXClientXMPP(
            msg.jid.strip(NB).decode("utf8"), msg.passwd.strip(NB).decode("utf8")
        )
        c["xmpp_session"] = xmpp_session
        xmpp_session.connect()
        # experimental sleep to have time to connect.
        # explicit wait with exponential backoff and timeout would be better
    logger.debug("Waiting for xmpp connection")
    timeout = 0
    while not xmpp_session.is_connected() and timeout <= 8:
        timeout += 0.5
        await asyncio.sleep(0.5)
    if not xmpp_session.is_connected():
        errmsg = b"Login failed. Check internet connection or credentials!"
        resp = InXMPPLoginResponse(
            status_code=InXMPPLoginResponse.ERROR, message=errmsg
        )
        return b"\x41", resp
    logger.debug("XMPP TCP stream created. Waiting for authentication")
    timeout = 0
    logger.debug("Waiting for xmpp authentication")
    while not xmpp_session.authenticated and timeout <= 8:
        timeout += 0.5
        await asyncio.sleep(0.5)
    if not xmpp_session.authenticated:
        errmsg = b"login failed. Check your credentials!"
        logger.warning(errmsg.decode("utf8"))
        resp = InXMPPLoginResponse(
            status_code=InXMPPLoginResponse.ERROR, message=errmsg
        )
        return b"\x41", resp
    # TODO: handle the connection/authentication in more general way
    # publish pubkey to my vcard

    vcard = VCardTemp()
    vcard["UID"] = hexlify(msg.pubkey).decode("utf8")
    set_job = await xmpp_session.plugin["xep_0054"].publish_vcard(vcard)
    # TODO: could raise exception
    resp = InXMPPLoginResponse(status_code=InXMPPLoginResponse.OK, message=b"login OK")
    return b"\x41", resp


@register_handler(b"\x42", OutAddContact)
async def handle_add_contact(msg, c):
    resp = InAddContact()
    # bytes
    resp.jid = msg.jid
    # string
    target_jid = msg.jid.strip(NB).decode("utf8")
    try:
        xmpp_session = c["xmpp_session"]
    except KeyError:
        resp = InAddContact

    try:
        target_vc = await xmpp_session.plugin["xep_0054"].get_vcard(jid=target_jid)
        resp.pubkey = unhexlify(target_vc["vcard_temp"]["UID"])
    except Exception as exc:
        logger.exception(exc)
        logger.error("No pubkey available for %s", target_jid)
        resp.pubkey = b""
    return b"\x43", resp


@register_handler(b"\x44", OutQueryUnread)
async def handle_get_number_of_messages(pb, c):
    # resp = await c['xmpp_session'].plugin['xep_0013'].get_count()
    # count = resp.xml.find(
    #     './{http://jabber.org/protocol/disco#info}query/'
    #     '{jabber:x:data}x/'
    #     '{jabber:x:data}field/[@var="number_of_messages"]/{jabber:x:data}value')
    xmpp = c["xmpp_session"]
    resp = InQueryUnread()
    await xmpp.get_offline_message_headers()
    resp.unread_nicks = xmpp.format_offline_message_headers_into_counts()
    return b"\x45", resp


@register_handler(b"\x46", OutQueryMessage)
async def handle_querying_message_contents(pb, c):
    xmpp = c["xmpp_session"]
    target_jid = pb.from_jid.strip(NB).decode("utf8")
    msg = await xmpp.get_message_for_jid(target_jid, delete=True)
    resp = InMessage()
    resp.jid = msg["from"].bare.encode("utf8").ljust(62, NB)
    try:
        msg_body, metadata = decode_message(msg["body"].encode("utf8"))
    except Exception as err:
        msg_body = msg["body"].encode("utf8").ljust(256, NB)
        metadata = NB * 32
    resp.message = msg_body
    resp.timestamp = int(msg["delay"]["stamp"].timestamp())
    resp.metadata = metadata
    resp.checksum = b"\xff" * 32
    return b"\x47", resp


# @register_handler(b'\x48', OutMessageAck)
# async def handle_deleting_processed_message(pb, c):
#     xmpp = c['xmpp_session']
#     target_message_id = pb.message_id.strip(NB).decode('utf8')
#     await xmpp.remove_offline_message(target_message_id)
#     resp = InMessageAck()
#     resp.jid = pb.jid
#     resp.action = resp.DELETE
#     return b'\x49', resp


@register_handler(b"\x50", OutMessage)
async def send_message(pb, c):
    xmpp = c["xmpp_session"]
    resp = InMessageAck()
    resp.jid = pb.jid
    target = pb.jid.strip(NB).decode("utf8")
    # logger.debugv("Sending message to %s", target)
    encoded_msg = encode_message(pb.message, pb.metadata)
    xmpp.send_message(mto=target, mbody=encoded_msg)
    # wait for error message to appear (if any)
    await asyncio.sleep(1)
    try:
        errmsg = xmpp.message_q.get_nowait()
        # there was erorr sending the message
        logger.info("Unable to send message, resp %s", errmsg)
        resp.action = resp.ERROR
    except asyncio.QueueEmpty:
        # assume message got sent because on success no event is emitted
        resp.action = resp.OK
    return b"\x51", resp
