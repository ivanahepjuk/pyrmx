import logging
import math
import struct
from binascii import unhexlify
import asyncio
from pyrmx.monero.node import MoneroRPCClient, PyrmxMoneroClient
from pyrmx.pyrmxd import register_handler
from pyrmx.pb_schemas import monero_pb2, system_messages_pb2
from pyrmx.exceptions import BlockheightNotFound
from pyrmx.monero.utils import (
    Transaction,
    TransactionBatch,
    AsyncBlockManager,
    AsyncBatchManager,
    BatchIndex,
)
from pyrmx.pyrmxd import register_handler
from pyrmx.utils import NB

logger = logging.getLogger(__name__)


@register_handler(b"\x84", monero_pb2.OutInitScanning)
async def handle_init_scanning(pb, c):
    c["xmr_scanning_batch_size"] = pb.batch_size
    resp = monero_pb2.InInitScanning()
    # TODO: should be non-blocking
    resp.global_blockheight = (await c["xmr_node"].get_info())["result"]["height"]
    return b"\x85", resp


@register_handler(b"\x86", monero_pb2.OutGetTransactionBatch)
async def handle_sending_xmr_transaction_batches(pb, c):
    # todo must exist at this point
    xmr = c["xmr_node"]

    want_height = pb.local_blockheight
    want_offset = pb.offset

    logger.debug("Want batch %s", pb)
    # get or setup new feed
    try:
        batch_manager = c["xmr_batch_manager"]
    except KeyError:
        c["xmr_block_manager"] = block_manager = AsyncBlockManager(xmr)
        c["xmr_batch_manager"] = batch_manager = AsyncBatchManager(block_manager)
        for future_bh in range(want_height, want_height + 10):
            block_manager.schedule_block(future_bh)

    # TODO: add pruning of batch cache, maybe by leveraging
    # c.get('xmr_previous_batch') = batch
    try:
        want_batch_ix = BatchIndex(want_height, want_offset)
        batch = await batch_manager.get_batch(
            want_batch_ix, size=c["xmr_scanning_batch_size"], prefetch_next=True
        )
        batch_manager.delete_batch(want_batch_ix, c["xmr_scanning_batch_size"])
    except BlockheightNotFound as err:
        logger.info(err)
        logger.info("Stop scanning")
        batch = TransactionBatch(max_size=c["xmr_scanning_batch_size"])
    # we store the previous batch in case there is a match
    # the batch is indexable by tx offset
    # (the offset is only relative to this particular batch)
    # this could be refactored such that we send to rmx the absolute batch identifier
    # (start bheight, start tx_offset + size)
    resp = monero_pb2.InGetTransactionBatch()
    # no more txns in block

    try:
        last_batch_tx = batch[-1]
        resp.end_blockheight = last_batch_tx.block_height
        resp.end_tx_offset = last_batch_tx.tx_offset
    except IndexError:
        # if there are no transactions in batch return the original indexes
        # as the next indexes
        resp.end_blockheight = want_height
        resp.end_tx_offset = want_offset

    resp.start_blockheight = want_height
    resp.start_tx_offset = want_offset

    next_ix = batch.next_batch_index
    resp.next_blockheight = next_ix.blockheight
    resp.next_tx_offset = next_ix.tx_offset

    if c["xmr_block_manager"].global_blockheight == math.inf:
        logger.warning(
            "Unknown cached global blockheight. If this "
            "happens often this is probably a bug"
        )
        height = (await c["xmr_node"].get_info())["result"]["height"]
    else:
        height = c["xmr_block_manager"].global_blockheight

    resp.global_blockheight = height
    resp.data = batch.as_rmx_bytes()
    # send message
    logger.info(
        "Sending %s ending at blockeight %s offset %s",
        batch,
        resp.end_blockheight,
        resp.end_tx_offset,
    )
    return b"\x87", resp


@register_handler(b"\x88", monero_pb2.OutGetTransactionDetails)
async def handle_get_transaction_details(request, c):

    batch = await c["xmr_batch_manager"].get_batch(
        BatchIndex(request.batch_blockheight, request.batch_tx_offset),
        c["xmr_scanning_batch_size"],
        prefetch_next=False,
    )

    resp = monero_pb2.InGetTransactionDetails()
    target_tx = batch[request.want_tx_offset]
    resp.TxID = unhexlify(target_tx.tx_hash)

    try:
        tx_type = target_tx["as_json"]["rct_signatures"]["type"]
    except KeyError:
        # coinbase tx doesn't have rct_signatures field
        # which is not always the case ??
        tx_type = 0

    if tx_type == 0:
        resp.type = resp.TransactionType.COINBASE
        amounts = []
        for amount in target_tx["as_json"]["vout"]:
            try:
                int(amount["amount"])
            except TypeError:
                raise ValueError(
                    "Expected coinbase transaction {} but the amount {} is encrypted"
                    .format(request.TxID, amount))
            else:
                amounts.append(struct.pack("<Q", amount["amount"]))
    elif tx_type == 4:
        resp.type = resp.TransactionType.RCT_4
        amounts = [
            unhexlify(i["amount"])
            for i
            in target_tx["as_json"]["rct_signatures"]["ecdhInfo"]
        ]
    else:
        raise ValueError(
            "Unknown transaction type {}. Check tx_hash {}".format(
                tx_type, request.TxID))

    # +/* Getting TXID details: Number of P, P0...Px, ECDHINFO0 ..... ECDHINFOx
    tx_data = b"".join(target_tx.R_keys)

    for p_key, amount in zip(target_tx.P_keys, amounts):
        tx_data += p_key + amount
    resp.Rnum = len(target_tx.R_keys)
    resp.Pnum = len(target_tx.P_keys)
    resp.Tx_data = tx_data.ljust(c['xmr_scanning_batch_size'], NB)
    return b"\x89", resp


@register_handler(b"\x92", monero_pb2.OutIsSpent)
async def handle_is_spent(pb, c):
    result = await c["xmr_node"].is_key_image_spent([pb.key.decode("ascii")])

    resp = monero_pb2.InIsSpent()
    resp.spent = result["spent_status"][0]
    return b"\x93", resp
