"""
Everything related to external monero stuff
- communicating with monerod over RPC
- ...
"""

import logging
import json
from typing import List, Tuple, Optional, Dict
from pyrmx.exceptions import MoneroRPCError, BlockheightNotFound
from collections import namedtuple
from pyrmx.monero.utils import Block, Transaction
import aiohttp

logger = logging.getLogger(__name__)


class MoneroRPCClient:
    """Base client implementing low level communication with monerod

    pyrmx specific features are should be in a subclass
    """

    def __init__(self, monerod_url, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._session = aiohttp.ClientSession(raise_for_status=True)
        self.base_url = monerod_url.rstrip("/") + "/"
        logger.info("Connecting to Monero node at %s", monerod_url)

    async def close(self):
        await self._session.close()

    async def call(self, endpoint, payload):
        async with self._session.post(
            self.base_url + endpoint.lstrip("/"), json=payload
        ) as resp:
            # can't use resp.json() https://github.com/monero-project/monero/issues/6142
            rj = json.loads(await resp.text())
            if "error" in rj:
                raise MoneroRPCError(rj["error"]["message"], response=rj)
            else:
                return rj

    def _json_rpc_header(self, method, params=None):
        return {
            "jsonrpc": "2.0",
            "id": "0",
            "method": method,
            "params": params if params is not None else {},
        }

    async def get_block(self, height=None, hash_=None):
        """Get block by height or hash.
        The response 'json' is already serialized from string
        """
        if height is not None and hash_ is not None:
            raise ValueError("Choose either height or hash_ not both")
        if height is not None:
            params = {"height": height}
        else:
            params = {"hash": hash_}

        final_params = self._json_rpc_header("get_block", params)
        resp = await self.call("/json_rpc", final_params)
        resp["result"]["json"] = json.loads(resp["result"]["json"])
        return resp["result"]

    async def is_key_image_spent(self, key_images: List[str]):
        """
        https://www.getmonero.org/resources/developer-guides
        /daemon-rpc.html#is_key_image_spent
        """
        resp = await self.call("/is_key_image_spent", {"key_images": key_images})
        if resp["status"] != "OK":
            raise MoneroRPCError("Unable to check is_key_image_sepnt {}".format(resp))
        return resp

    async def get_transactions(self, txs_hashes: List[str], as_json=True):
        resp = await self.call(
            "/get_transactions", {"txs_hashes": txs_hashes, "decode_as_json": as_json}
        )
        # todo check status and missing txs
        if as_json:
            for txn in resp["txs"]:
                txn["as_json"] = json.loads(txn["as_json"])
            return resp
        else:
            return resp

    async def get_info(self):
        params = self._json_rpc_header("get_info")
        return await self.call("/json_rpc", payload=params)


class PyrmxMoneroClient(MoneroRPCClient):
    async def get_block_and_transactions(self, height):
        try:
            block = Block(await self.get_block(height=height))
        except MoneroRPCError as err:
            # https://github.com/monero-project/monero/blob/master/src/rpc/core_rpc_server_error_codes.h
            if err.response.get("error", {}).get("code") == -2:
                raise BlockheightNotFound(err, response=err.response)
            else:
                raise

        json_txns = await self.get_transactions(block.tx_hashes)
        block.transactions = [
            Transaction(tx_json=raw_tx, tx_offset=i)
            for i, raw_tx in enumerate(json_txns["txs"])
        ]
        return block
