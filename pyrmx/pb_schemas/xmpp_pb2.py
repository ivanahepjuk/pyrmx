# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: pyrmx/pb_schemas/xmpp.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='pyrmx/pb_schemas/xmpp.proto',
  package='',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n\x1bpyrmx/pb_schemas/xmpp.proto\";\n\x0cOutXMPPLogin\x12\x0b\n\x03jid\x18\x01 \x02(\x0c\x12\x0e\n\x06passwd\x18\x02 \x02(\x0c\x12\x0e\n\x06pubkey\x18\x03 \x02(\x0c\"}\n\x13InXMPPLoginResponse\x12\x34\n\x0bstatus_code\x18\x01 \x02(\x0e\x32\x1f.InXMPPLoginResponse.StatusCode\x12\x0f\n\x07message\x18\x02 \x02(\x0c\"\x1f\n\nStatusCode\x12\t\n\x05\x45RROR\x10\x00\x12\x06\n\x02OK\x10\x01\"\x1c\n\rOutAddContact\x12\x0b\n\x03jid\x18\x01 \x02(\x0c\"+\n\x0cInAddContact\x12\x0b\n\x03jid\x18\x01 \x02(\x0c\x12\x0e\n\x06pubkey\x18\x02 \x02(\x0c\"\x1f\n\x0eOutQueryUnread\x12\r\n\x05\x64ummy\x18\x01 \x02(\r\"%\n\rInQueryUnread\x12\x14\n\x0cunread_nicks\x18\x01 \x02(\x0c\"#\n\x0fOutQueryMessage\x12\x10\n\x08\x66rom_jid\x18\x01 \x02(\x0c\"`\n\tInMessage\x12\x11\n\ttimestamp\x18\x01 \x02(\r\x12\x0b\n\x03jid\x18\x02 \x02(\x0c\x12\x0f\n\x07message\x18\x03 \x02(\x0c\x12\x10\n\x08metadata\x18\x04 \x02(\x0c\x12\x10\n\x08\x63hecksum\x18\x05 \x02(\x0c\"]\n\rOutMessageAck\x12\x0b\n\x03jid\x18\x01 \x02(\x0c\x12\"\n\x06\x61\x63tion\x18\x02 \x02(\x0e\x32\x12.OutMessageAck.ACK\"\x1b\n\x03\x41\x43K\x12\n\n\x06\x44\x45LETE\x10\x00\x12\x08\n\x04KEEP\x10\x01\"a\n\nOutMessage\x12\x11\n\ttimestamp\x18\x01 \x02(\r\x12\x0b\n\x03jid\x18\x02 \x02(\x0c\x12\x0f\n\x07message\x18\x03 \x02(\x0c\x12\x10\n\x08metadata\x18\x04 \x02(\x0c\x12\x10\n\x08\x63hecksum\x18\x05 \x02(\x0c\"X\n\x0cInMessageAck\x12\x0b\n\x03jid\x18\x01 \x02(\x0c\x12!\n\x06\x61\x63tion\x18\x02 \x02(\x0e\x32\x11.InMessageAck.ACK\"\x18\n\x03\x41\x43K\x12\x06\n\x02OK\x10\x00\x12\t\n\x05\x45RROR\x10\x01')
)



_INXMPPLOGINRESPONSE_STATUSCODE = _descriptor.EnumDescriptor(
  name='StatusCode',
  full_name='InXMPPLoginResponse.StatusCode',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='ERROR', index=0, number=0,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='OK', index=1, number=1,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=186,
  serialized_end=217,
)
_sym_db.RegisterEnumDescriptor(_INXMPPLOGINRESPONSE_STATUSCODE)

_OUTMESSAGEACK_ACK = _descriptor.EnumDescriptor(
  name='ACK',
  full_name='OutMessageAck.ACK',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='DELETE', index=0, number=0,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='KEEP', index=1, number=1,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=567,
  serialized_end=594,
)
_sym_db.RegisterEnumDescriptor(_OUTMESSAGEACK_ACK)

_INMESSAGEACK_ACK = _descriptor.EnumDescriptor(
  name='ACK',
  full_name='InMessageAck.ACK',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='OK', index=0, number=0,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='ERROR', index=1, number=1,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=759,
  serialized_end=783,
)
_sym_db.RegisterEnumDescriptor(_INMESSAGEACK_ACK)


_OUTXMPPLOGIN = _descriptor.Descriptor(
  name='OutXMPPLogin',
  full_name='OutXMPPLogin',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='jid', full_name='OutXMPPLogin.jid', index=0,
      number=1, type=12, cpp_type=9, label=2,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='passwd', full_name='OutXMPPLogin.passwd', index=1,
      number=2, type=12, cpp_type=9, label=2,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='pubkey', full_name='OutXMPPLogin.pubkey', index=2,
      number=3, type=12, cpp_type=9, label=2,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=31,
  serialized_end=90,
)


_INXMPPLOGINRESPONSE = _descriptor.Descriptor(
  name='InXMPPLoginResponse',
  full_name='InXMPPLoginResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='status_code', full_name='InXMPPLoginResponse.status_code', index=0,
      number=1, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='message', full_name='InXMPPLoginResponse.message', index=1,
      number=2, type=12, cpp_type=9, label=2,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
    _INXMPPLOGINRESPONSE_STATUSCODE,
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=92,
  serialized_end=217,
)


_OUTADDCONTACT = _descriptor.Descriptor(
  name='OutAddContact',
  full_name='OutAddContact',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='jid', full_name='OutAddContact.jid', index=0,
      number=1, type=12, cpp_type=9, label=2,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=219,
  serialized_end=247,
)


_INADDCONTACT = _descriptor.Descriptor(
  name='InAddContact',
  full_name='InAddContact',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='jid', full_name='InAddContact.jid', index=0,
      number=1, type=12, cpp_type=9, label=2,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='pubkey', full_name='InAddContact.pubkey', index=1,
      number=2, type=12, cpp_type=9, label=2,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=249,
  serialized_end=292,
)


_OUTQUERYUNREAD = _descriptor.Descriptor(
  name='OutQueryUnread',
  full_name='OutQueryUnread',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='dummy', full_name='OutQueryUnread.dummy', index=0,
      number=1, type=13, cpp_type=3, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=294,
  serialized_end=325,
)


_INQUERYUNREAD = _descriptor.Descriptor(
  name='InQueryUnread',
  full_name='InQueryUnread',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='unread_nicks', full_name='InQueryUnread.unread_nicks', index=0,
      number=1, type=12, cpp_type=9, label=2,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=327,
  serialized_end=364,
)


_OUTQUERYMESSAGE = _descriptor.Descriptor(
  name='OutQueryMessage',
  full_name='OutQueryMessage',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='from_jid', full_name='OutQueryMessage.from_jid', index=0,
      number=1, type=12, cpp_type=9, label=2,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=366,
  serialized_end=401,
)


_INMESSAGE = _descriptor.Descriptor(
  name='InMessage',
  full_name='InMessage',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='timestamp', full_name='InMessage.timestamp', index=0,
      number=1, type=13, cpp_type=3, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='jid', full_name='InMessage.jid', index=1,
      number=2, type=12, cpp_type=9, label=2,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='message', full_name='InMessage.message', index=2,
      number=3, type=12, cpp_type=9, label=2,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='metadata', full_name='InMessage.metadata', index=3,
      number=4, type=12, cpp_type=9, label=2,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='checksum', full_name='InMessage.checksum', index=4,
      number=5, type=12, cpp_type=9, label=2,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=403,
  serialized_end=499,
)


_OUTMESSAGEACK = _descriptor.Descriptor(
  name='OutMessageAck',
  full_name='OutMessageAck',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='jid', full_name='OutMessageAck.jid', index=0,
      number=1, type=12, cpp_type=9, label=2,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='action', full_name='OutMessageAck.action', index=1,
      number=2, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
    _OUTMESSAGEACK_ACK,
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=501,
  serialized_end=594,
)


_OUTMESSAGE = _descriptor.Descriptor(
  name='OutMessage',
  full_name='OutMessage',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='timestamp', full_name='OutMessage.timestamp', index=0,
      number=1, type=13, cpp_type=3, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='jid', full_name='OutMessage.jid', index=1,
      number=2, type=12, cpp_type=9, label=2,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='message', full_name='OutMessage.message', index=2,
      number=3, type=12, cpp_type=9, label=2,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='metadata', full_name='OutMessage.metadata', index=3,
      number=4, type=12, cpp_type=9, label=2,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='checksum', full_name='OutMessage.checksum', index=4,
      number=5, type=12, cpp_type=9, label=2,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=596,
  serialized_end=693,
)


_INMESSAGEACK = _descriptor.Descriptor(
  name='InMessageAck',
  full_name='InMessageAck',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='jid', full_name='InMessageAck.jid', index=0,
      number=1, type=12, cpp_type=9, label=2,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='action', full_name='InMessageAck.action', index=1,
      number=2, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
    _INMESSAGEACK_ACK,
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=695,
  serialized_end=783,
)

_INXMPPLOGINRESPONSE.fields_by_name['status_code'].enum_type = _INXMPPLOGINRESPONSE_STATUSCODE
_INXMPPLOGINRESPONSE_STATUSCODE.containing_type = _INXMPPLOGINRESPONSE
_OUTMESSAGEACK.fields_by_name['action'].enum_type = _OUTMESSAGEACK_ACK
_OUTMESSAGEACK_ACK.containing_type = _OUTMESSAGEACK
_INMESSAGEACK.fields_by_name['action'].enum_type = _INMESSAGEACK_ACK
_INMESSAGEACK_ACK.containing_type = _INMESSAGEACK
DESCRIPTOR.message_types_by_name['OutXMPPLogin'] = _OUTXMPPLOGIN
DESCRIPTOR.message_types_by_name['InXMPPLoginResponse'] = _INXMPPLOGINRESPONSE
DESCRIPTOR.message_types_by_name['OutAddContact'] = _OUTADDCONTACT
DESCRIPTOR.message_types_by_name['InAddContact'] = _INADDCONTACT
DESCRIPTOR.message_types_by_name['OutQueryUnread'] = _OUTQUERYUNREAD
DESCRIPTOR.message_types_by_name['InQueryUnread'] = _INQUERYUNREAD
DESCRIPTOR.message_types_by_name['OutQueryMessage'] = _OUTQUERYMESSAGE
DESCRIPTOR.message_types_by_name['InMessage'] = _INMESSAGE
DESCRIPTOR.message_types_by_name['OutMessageAck'] = _OUTMESSAGEACK
DESCRIPTOR.message_types_by_name['OutMessage'] = _OUTMESSAGE
DESCRIPTOR.message_types_by_name['InMessageAck'] = _INMESSAGEACK
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

OutXMPPLogin = _reflection.GeneratedProtocolMessageType('OutXMPPLogin', (_message.Message,), {
  'DESCRIPTOR' : _OUTXMPPLOGIN,
  '__module__' : 'pyrmx.pb_schemas.xmpp_pb2'
  # @@protoc_insertion_point(class_scope:OutXMPPLogin)
  })
_sym_db.RegisterMessage(OutXMPPLogin)

InXMPPLoginResponse = _reflection.GeneratedProtocolMessageType('InXMPPLoginResponse', (_message.Message,), {
  'DESCRIPTOR' : _INXMPPLOGINRESPONSE,
  '__module__' : 'pyrmx.pb_schemas.xmpp_pb2'
  # @@protoc_insertion_point(class_scope:InXMPPLoginResponse)
  })
_sym_db.RegisterMessage(InXMPPLoginResponse)

OutAddContact = _reflection.GeneratedProtocolMessageType('OutAddContact', (_message.Message,), {
  'DESCRIPTOR' : _OUTADDCONTACT,
  '__module__' : 'pyrmx.pb_schemas.xmpp_pb2'
  # @@protoc_insertion_point(class_scope:OutAddContact)
  })
_sym_db.RegisterMessage(OutAddContact)

InAddContact = _reflection.GeneratedProtocolMessageType('InAddContact', (_message.Message,), {
  'DESCRIPTOR' : _INADDCONTACT,
  '__module__' : 'pyrmx.pb_schemas.xmpp_pb2'
  # @@protoc_insertion_point(class_scope:InAddContact)
  })
_sym_db.RegisterMessage(InAddContact)

OutQueryUnread = _reflection.GeneratedProtocolMessageType('OutQueryUnread', (_message.Message,), {
  'DESCRIPTOR' : _OUTQUERYUNREAD,
  '__module__' : 'pyrmx.pb_schemas.xmpp_pb2'
  # @@protoc_insertion_point(class_scope:OutQueryUnread)
  })
_sym_db.RegisterMessage(OutQueryUnread)

InQueryUnread = _reflection.GeneratedProtocolMessageType('InQueryUnread', (_message.Message,), {
  'DESCRIPTOR' : _INQUERYUNREAD,
  '__module__' : 'pyrmx.pb_schemas.xmpp_pb2'
  # @@protoc_insertion_point(class_scope:InQueryUnread)
  })
_sym_db.RegisterMessage(InQueryUnread)

OutQueryMessage = _reflection.GeneratedProtocolMessageType('OutQueryMessage', (_message.Message,), {
  'DESCRIPTOR' : _OUTQUERYMESSAGE,
  '__module__' : 'pyrmx.pb_schemas.xmpp_pb2'
  # @@protoc_insertion_point(class_scope:OutQueryMessage)
  })
_sym_db.RegisterMessage(OutQueryMessage)

InMessage = _reflection.GeneratedProtocolMessageType('InMessage', (_message.Message,), {
  'DESCRIPTOR' : _INMESSAGE,
  '__module__' : 'pyrmx.pb_schemas.xmpp_pb2'
  # @@protoc_insertion_point(class_scope:InMessage)
  })
_sym_db.RegisterMessage(InMessage)

OutMessageAck = _reflection.GeneratedProtocolMessageType('OutMessageAck', (_message.Message,), {
  'DESCRIPTOR' : _OUTMESSAGEACK,
  '__module__' : 'pyrmx.pb_schemas.xmpp_pb2'
  # @@protoc_insertion_point(class_scope:OutMessageAck)
  })
_sym_db.RegisterMessage(OutMessageAck)

OutMessage = _reflection.GeneratedProtocolMessageType('OutMessage', (_message.Message,), {
  'DESCRIPTOR' : _OUTMESSAGE,
  '__module__' : 'pyrmx.pb_schemas.xmpp_pb2'
  # @@protoc_insertion_point(class_scope:OutMessage)
  })
_sym_db.RegisterMessage(OutMessage)

InMessageAck = _reflection.GeneratedProtocolMessageType('InMessageAck', (_message.Message,), {
  'DESCRIPTOR' : _INMESSAGEACK,
  '__module__' : 'pyrmx.pb_schemas.xmpp_pb2'
  # @@protoc_insertion_point(class_scope:InMessageAck)
  })
_sym_db.RegisterMessage(InMessageAck)


# @@protoc_insertion_point(module_scope)
