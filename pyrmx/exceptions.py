class RmxError(Exception):
    pass


class TransactionBatchSizeReached(RmxError):
    """Raised when adding transaction to a batch and the txn would not fit """

    pass


class RmxUserError(RmxError):
    pass


class RmxHwError(RmxError):
    pass


class RmxHwNotConnected(RmxHwError):
    pass


class RmxNotAuthenticated(RmxError):
    pass


class MalformedDataError(RmxError, ValueError):
    def __init__(self, message, raw_blob=None):
        super().__init__(message)
        self.raw_blob = raw_blob


class MoneroRPCError(Exception):
    def __init__(self, message, response=None):
        super().__init__(message)
        self.response = response


class BlockheightNotFound(MoneroRPCError):
    pass
