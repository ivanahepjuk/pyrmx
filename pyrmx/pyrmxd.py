import time
import asyncio
import functools
import logging
import struct
from binascii import hexlify

from pyrmx.exceptions import MalformedDataError, RmxHwError
from pyrmx.device import RMXDevice
from pyrmx.messages import CMD_START, Message, _ensure_bytes
from pyrmx.pb_schemas.system_messages_pb2 import OutDebug

from collections import namedtuple
import logging

logger = logging.getLogger(__name__)
HANDLERS = {}

CommandHandler = namedtuple("CommandHandler", "callback, proto")


def register_handler(command_byte, ProtoClass):
    """Decorator to pair a command byte to it's ProtoBuf

        a the wrapped function (callback) takes the proto message has signature
        def callback(pb: ProtoBufMessage): -> Tuple[byte, ProtoBufMessage]
        i.e. takes the request protobuf message and replies with a command byte
        and a response protobuf message

    use like
    @register_handler(b'\xff', 'OutProtoMessage')
    def react_to_xff(message):
        print("Got message", message)
        return b'\xff', InProtoMessage()
    """

    def decorated(func):
        if command_byte in HANDLERS:
            raise ValueError(
                "command_byte {!r} is already handled by {}".format(
                    command_byte, HANDLERS
                )
            )
        logger.debug(
            "Registering command byte {!r} with function {}".format(command_byte, func)
        )
        HANDLERS[command_byte] = CommandHandler(callback=func, proto=ProtoClass)
        return func

    return decorated


@register_handler(b"\xff", OutDebug)
async def handle_debug_message(pb, c):
    logger.debug("Debug message from RMX:\n%s", pb.message.decode("ascii"))
    return None, None


class PyRMXDaemon:
    """
    Top level daemon handling requests from RMX hw
    """

    def __init__(self, context=None, device=None):
        self.device = device or RMXDevice()
        self.device.flush()
        self.context = context or {}

    async def run(self):
        try:
            await self._run()
        finally:
            await self.cleanup()

    async def cleanup(self):
        await self.context["xmr_node"].close()

    async def _run(self):
        logger.info("Starting daemon")
        while True:
            msg = await self.read_message()
            logger.debug("Got command byte %s", hexlify(msg.command_byte))
            logger.debugvv("Got msg %s", hexlify(msg.as_bytes()))
            handler = HANDLERS[msg.command_byte]
            resp_byte, resp_proto = await handler.callback(msg.payload, self.context)
            if resp_byte is None:
                # debug message
                continue
            response = Message.build(
                resp_byte, payload_bytes=resp_proto.SerializeToString()
            )
            response_bytes = response.as_bytes()
            # logger.debugvv("RESP: %s", hexlify(response_bytes))
            logger.debug("Sending response")
            self.device.send_bytes(response_bytes)
            logger.debug("Responded with %s", resp_byte)

    async def read_message(self):

        # wait until start of command byte
        logger.debug("Waiting for command (start byte) %s", CMD_START)
        start_byte = await self.device.read_bytes(1)
        while start_byte != CMD_START:
            logger.warning(
                "got byte %s but it's not %s", hexlify(start_byte), hexlify(CMD_START)
            )
            start_byte = await self.device.read_bytes(1)
        blob = b""
        logger.debugvv("Got start byte %s", start_byte)
        blob += start_byte
        # length of the rest
        command_byte = await self.device.read_bytes(1)
        blob += command_byte
        raw_payload_size = await self.device.read_bytes(2)
        blob += raw_payload_size
        # actual data size as 2byte int
        payload_size = struct.unpack(">H", raw_payload_size)[0]
        payload_bytes = await self.device.read_bytes(payload_size)
        blob += payload_bytes

        checksum = await self.device.read_bytes(8)

        message = Message(
            start_byte=start_byte,
            command_byte=command_byte,
            payload_size=payload_size,
            payload_bytes=payload_bytes,
            checksum=checksum,
            raw_blob=blob,
        )
        message.verify()
        try:
            handler = HANDLERS[_ensure_bytes(message.command_byte)]
        except KeyError:
            raise RmxHwError(
                "Unknown command byte {!r} received, payload {!r}".format(
                    message.command_byte, message.payload_bytes
                )
            )
        message.payload = handler.proto().FromString(message.payload_bytes)
        return message
