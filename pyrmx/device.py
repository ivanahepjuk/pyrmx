import asyncio
import time
import random
import sys

from pylibftdi import Driver, Device, SerialDevice
from hashlib import sha256
from binascii import hexlify
from pylibftdi import FtdiError
from pyrmx.exceptions import RmxHwNotConnected
import logging

logger = logging.getLogger(__name__)


class RMXDevice(Device):
    """Class for computer HW interface and low level communication
    """

    def __init__(self):
        # ftdi driver
        try:
            super().__init__(
                device_id="524D58", mode="b", interface_select=2
            )  # uncomment when not using serial port anymore
        except FtdiError as err:
            raise RmxHwNotConnected(
                "RMX not connected. Please connect RMX device and then try again"
            )
        else:
            logger.info("RMX device connected")
            self.baudrate = 115200
            self.flush()

    def send_bytes(self, payload):
        # a synchronous code but there is no way around that actually
        # maybe split the input into chunks and write the chunks separately
        # but we don't really care
        return self.write(payload)

    async def read_bytes(self, n=4096):
        response = b""
        remaining = n
        while remaining > 0:
            chunk = self.read(remaining)
            response += chunk
            remaining -= len(chunk)
            await asyncio.sleep(0.001)
        return response
